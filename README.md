# HIFIS Newsletter Statistics

Basic statistics on HIFIS Newsletter.

- number of subscribers

## update interval

- irregularly, manually.

## plot
[here](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/file/plots/pdf/newsletter-plot_2.pdf?job=plot_general)

## Further infos

- Newsletter is being sent quarterly since July 2022
- see also <https://hifis.net/news/2022/07/29/Newsletter>
